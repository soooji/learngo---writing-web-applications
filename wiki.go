/*
These codes are write following below instruction:
https://golang.org/doc/articles/wiki/#tmp_0
By: Sajad Beheshti
*/

package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
)

type Page struct { // Structure of a Page
	Title string
	Body []byte
}

var templates = template.Must(template.ParseFiles("edit.html", "view.html", "404.html")) //prevents repeat in template parses
var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-Z0-9]+)$")

func (p *Page) save() error { // saves the file via WriteFIle method
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename,p.Body,0600)
}
func loadPage(title string, fileType string) (*Page, error) { //load a page : Title and Body
	filename := title + "." + fileType
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title:title,Body:body}, nil
}


func editHandler(w http.ResponseWriter, r *http.Request, title string) { //handle edit url: first loads the page, then render that in edit template
	p, err := loadPage(title,"txt")
	if err != nil { //if it doesn't exist, create a new page with the url title
		p = &Page{Title: title}
	}
	renderTemplate(w,"edit",p)
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) { //handle view a file: first loads the page, then render in view template
	p, err := loadPage(title,"txt")
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w,"view",p)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) { // reads the form request(r) values then create a new page and call save function
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w,err.Error(),http.StatusInternalServerError)
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) { // get and write data in suitable template
	err := templates.ExecuteTemplate(w,tmpl+".html",p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}


func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			//http.NotFound(w, r)
			errorHandler(w,r,http.StatusNotFound)
			return
		}
		fn(w, r, m[2])
	}
}
func homeHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		errorHandler(w, r, http.StatusNotFound)
		return
	}
	p,_:=loadPage("home","html")
	_, _ = fmt.Fprint(w, string(p.Body))
}

func errorHandler(w http.ResponseWriter, r *http.Request, status int) {
	w.WriteHeader(status)
	if status == http.StatusNotFound {
		p,_:=loadPage("404","html")
		_, _ = fmt.Fprint(w, string(p.Body))
	}
}
func main() {
	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))
	http.HandleFunc("/", homeHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}